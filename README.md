# Fingerspill #
===============



## What's this? ##

This is a touch-based, cross-platform, child-proof music player. 

Light on dependencies, no configuration, magically finding your content. Just start it up, touch something, and it plays.

Oh no! not another player, you say. My answer: I've never seen a music player as simple, easy and fit for kids as the ios6 music 
app. So this is my attempt at recreating that experience.



## Dependencies ##

Python: PyQt4, Pony (ORM), mutagen (music tag reader)
