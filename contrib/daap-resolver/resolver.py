#!/usr/bin/python2
# -*- coding: utf-8 -*-
#
# Created by Christophe "Tito" De Wolf <tito@webtito.be> twitter.com/tito1337
# Licensed under GPLv3 (http://www.gnu.org/licenses/gpl)
######################################################################

import sys
import re
from struct import unpack, pack
import json
from daap import DAAPClient
import logging
import difflib
from collections import Iterable

###################################################################### config
DAAP_HOST = "192.168.0.101"
DAAP_PORT = "3689"

###################################################################### logger
# TODO : disable this when fully tested
logger = logging.getLogger('daap-resolver')
hdlr = logging.FileHandler('daap-resolver.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

logger.info('Started')


###################################################################### resolver
class DAAPresolver:
    def __init__(self, host, port=None):
        self.host = host
        if port is None:
            port = "3689"
        self.port = port
        self.client = DAAPClient()
        self.client.connect(host, port)
        logger.info("Connected to %s:%s"%(host,port))
        self.session = self.client.login()

        databases = self.session.databases()
        for d in databases:
            if str(d.id) == str(self.session.library().id):
                self.database = d

        self.tracks = self.database.tracks()
        logger.info("Got %s tracks"%len(self.tracks))

    def fulltext(self, search):
        logger.info('Searching %s in %d tracks'%(search, len(self.tracks)))
        seqMatch = difflib.SequenceMatcher(None, "foobar", search)
        for t in self.tracks:
            seqMatch.set_seq1(t.artist)
            score = seqMatch.quick_ratio()
            seqMatch.set_seq1(t.album)
            score = max( seqMatch.quick_ratio(),  score )
            seqMatch.set_seq1(t.name)
            score = max( seqMatch.quick_ratio(),  score )
            if score >= 0.3:
                yield t

    def artistandtrack(self, artist, track):
        logger.info('Searching %s - %s in %d tracks'%(artist, track, len(self.tracks)))
        seqMatchArtist = difflib.SequenceMatcher(None, "foobar", self.stripFeat(artist))
        seqMatchTrack = difflib.SequenceMatcher(None, "foobar", self.stripFeat(track))
        for t in self.tracks:
            seqMatchArtist.set_seq1(self.stripFeat(t.artist))
            seqMatchTrack.set_seq1(self.stripFeat(t.name))
            scoreArtist = seqMatchArtist.quick_ratio()
            scoreTrack = seqMatchTrack.quick_ratio()
            score = (scoreArtist + scoreTrack) /2
            if score >= 0.85:
                logger.info("%s - %s : %s - %s : %f,%f,%s"%(artist, track, t.artist, t.name, scoreArtist, scoreTrack, score))
                yield t

    def genre(self, genres):
        if not isinstance(genres, Iterable): 
            genres = (genres, )
        g = list((_g.lower() for _g in genres))
        logger.info('Searching genre %s  in %d tracks'%(genres, len(self.tracks)))
        for t in self.tracks:
           if t.genre is not None and t.genre.lower() in g: 
                logger.info("%s : %s - %s : %s"%(genres, t.artist, t.name, t.genre))
                yield t

    def stripFeat(self,  s):
        patterns = ['^(.*?)\(feat\..*?\).*?$',  '^(.*?)feat\..*?$']
        for pattern in patterns:
            reg = re.search(pattern,  s)
            if reg:
                s= reg.group(1)
        return s
##################################################################### main
if __name__ == '__main__':
    resolver = DAAPresolver(DAAP_HOST, DAAP_PORT)
    print list(resolver.genre(sys.argv[1:]))
    
