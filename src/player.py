# -*- coding: utf-8 -*-

# A short script to play audio
# original: https://gitorious.org/plonk

# The code is licensed under the GPL, version 2 or later
# Copyright Håvard Gulldahl <havard@gulldahl.no>, 2009, 2013

import sys, os.path
from PyQt4 import QtCore, QtGui
from PyQt4.phonon import Phonon

class plonkPlayer(object):
  popuplog = []
  def __init__(self, app, cutoff=None):
    self.cutoff = cutoff
    self.player = Phonon.MediaObject(app)
    self.speaker = Phonon.AudioOutput(Phonon.MusicCategory, app)
    self.wire = Phonon.createPath(self.player, self.speaker)
    app.connect(self.player, QtCore.SIGNAL('totalTimeChanged(qint64)'), self.nowPlaying)
    self.player.setTickInterval(1000) # emit tick() every second
    app.connect(self.player, QtCore.SIGNAL('tick(qint64)'), self.tick)
  def enqueue(self, soundSources):
    for source in soundSources:
      self.player.enqueue(Phonon.MediaSource(source))
  def play(self):
    self.player.play()
    #print "interval: %s" % self.player.tickInterval()
  def nowPlaying(self, totaltime=1):
    sound = unicode(self.player.currentSource().url().toString())
    if sound in self.popuplog: return # we've already showed a popup for this clip
    verb = self.cutoff and "Prelistening to %ss of" % self.cutoff or "Playing"
    msg = "%s %s... (%s)" % (verb, os.path.basename(sound), self.formatTime(totaltime))
#method int org.kde.KNotify.event(QString event, QString fromApp, QVariantList contexts, QString text, QByteArray pixmap, QStringList actions, qlonglong winId)
    #i = self.display.event("notification", "kde", [], msg, [0,0,0,0], [], 0, dbus_interface="org.kde.KNotify")
    print msg
    self.popuplog.append(sound)
    timeout = self.cutoff*1000 - 500
    if timeout > totaltime:
      #print "timeout (%s) > totaltime (%s)" % (timeout, totaltime)
      timeout = totaltime - 500
  def closeNowPlaying(self, noteid):
    self.display.closeNotification(noteid)
  def formatTime(self, millisecs):
    secs = millisecs/1000 % 60
    mins = int(millisecs/1000) / 60
    return '%.2i:%.2i' % (mins, secs)
  def tick(self, millisecs):
    #print "tick: %s" % millisecs
    if self.cutoff is None: return
    if millisecs > self.cutoff*1000:
      #end of prelisten reached
      next = self.next()
      if next is not None:
        self.player.setCurrentSource(next)
        self.player.play()
      else:
        self.player.stop()
        self.player.emit(QtCore.SIGNAL('finished()'))
  def next(self):
    queue = self.player.queue()
    #print "q: %s" % repr(queue)
    if len(queue) == 0: return None
    for q in queue:
      if q != self.player.currentSource():
        return q
    return None

def gui():
  app = QtGui.QApplication(sys.argv)
  return app

def cli():
  app = QtCore.QCoreApplication(sys.argv)
  return app

if __name__ == '__main__':
  if len(sys.argv) == 1:
    print "Usage: %s <audio file(s)>" % sys.argv[0]
    sys.exit(1) 
  app = cli()
  app.setApplicationName('plonk')
  prelistenTime = 10 # prelisten 10 seconds of each clip
  if 'plonkplay' in sys.argv[0].lower():
    prelistenTime = None # play the whole clip
  try:
    player = plonkPlayer(app, cutoff=prelistenTime)
    player.enqueue(sys.argv[1:])
    player.play()
    app.connect(player.player, QtCore.SIGNAL('finished()'), app.quit)
    sys.exit(app.exec_())
  except Exception, (e):
    #logging.exception(e)
    app.quit()

# Qt Phonon reference:
# http://doc.trolltech.com/4.4/phonon-mediaobject.html
# PyQt4.Phonon reference:
# http://www.riverbankcomputing.co.uk/static/Docs/PyQt4/html/phonon-mediaobject.html

