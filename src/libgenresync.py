#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""Tool to cache every music file that fits a certain genre (given as argument to the script), to disk.

You may want to run this periodically, e.g. in your crontab. """


import sys
sys.path.insert(0, './../contrib/daap-resolver') # get the daap resolver
import resolver

DAAP_HOST = '192.168.0.101'
daap = resolver.DAAPresolver(DAAP_HOST)

for track in daap.genre(sys.argv[1:]):
    print("library/%s.%s" % (track.id, 'mp3'))#track.format))
    print [repr(z) for z in (track.artist, track.name, track.type, track.format, track.track, track.title, track.time, track.size)]
    track.save("library/%s.%s" % (track.id, 'mp3'))#track.type))



