#!/usr/bin/env python
# -*- encoding: utf-8 -*-


#############################################################################
##
## Copyright (C) 2010 Riverbank Computing Limited.
## Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
## All rights reserved.
##
## This file is part of the examples of PyQt.
##
## $QT_BEGIN_LICENSE:BSD$
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
## $QT_END_LICENSE$
##
#############################################################################
#
# 
# additional code (C) 2013 by havard@gulldahl.no, also available under 
# the BSD license on the terms listed above.
#



# This is only needed for Python v2 but is harmless for Python v3.
import sip
sip.setapi('QString', 2)

import sys
import os
from os.path import join, splitext
import itertools
import operator

import mutagen
from PyQt4 import QtCore, QtGui

#from pony.orm import *
from pony.orm import Database, Required, Optional, Set, count, db_session

try:
    from PyQt4.phonon import Phonon
except ImportError:
    app = QtGui.QApplication(sys.argv)
    QtGui.QMessageBox.critical(None, "Music Player",
            "Your Qt installation does not have Phonon support.",
            QtGui.QMessageBox.Ok | QtGui.QMessageBox.Default,
            QtGui.QMessageBox.NoButton)
    sys.exit(1)

class MetaDataParser(object):
    def __init__(self, filename):
        self.filename = unicode(filename)
        self.md = mutagen.File(unicode(filename))

    def pprint(self):
        print self.md
        self.md.pprint()

    def getField(self, key):
        try:
            f = self.md[key]
        except KeyError:
            try:
                f = self.md.tags[key + ":"]
            except KeyError:
                return None

        if hasattr(f, 'text'):
            return f.text
        elif hasattr(f, 'data'):
            return f.data
        return None

    def getFieldJoined(self, key, sep='; '):
        f = self.getField(key)
        if f is None: 
            return None
        else:
            return sep.join(f)

    def title(self):
        return self.getFieldJoined('TIT2') or 'Unknown title'

    def artist(self):
        return self.getFieldJoined('TPE1') or 'Unknown artist'

    def album(self):
        return self.getFieldJoined('TALB') or 'Unknown album'

    def cover(self):
        return self.getField('APIC')

    def track(self):
        trck = self.getFieldJoined('TRCK')
        if trck is None: return 0
        return int(trck.split('/')[0], 10)

db = Database('sqlite', 'albumstore.db', create_db=True)

class Album(db.Entity):
    # pony db model
    title = Required(unicode)
    artist = Optional(unicode)
    cover = Optional(buffer)
    tracks = Set("Track")

    def json(self):
        return {'title':self.title, 
                'artist':self.artist,
                'cover':self.cover,
                'tracks':list( [t.json() for t in self.tracks] )}

class Track(db.Entity):
    # pony db model
    trackno = Optional(int)
    title = Required(unicode)
    path = Optional(unicode)
    on = Required(Album)

    def json(self):
        return {'title':self.title, 
                'trackno':self.trackno,
                'path':self.path }

db.generate_mapping(create_tables=True)
@db_session
def loadAlbums(startidx=0, length=15):
    albums =  Album.select()[startidx:length]
    return [ a.json() for a in albums ]

@db_session
def totalAlbumCount():
    return count(a for a in Album)

@db_session
def addAlbums(albumList):
    #print "albumList: ", albumList
    for name, tracks in albumList.iteritems(): 
        A = Album.get(title=name) or Album(title=name)
        for t in tracks:    
            # t is instance of MetaDataParser
            if not A.artist:
                A.artist = t.artist()
            if not A.cover:
                A.cover = t.cover()
            T = Track.get(title=t.title(), on=A) or Track(title=t.title(), on=A)
            T.trackno=t.track()
            T.path=t.filename

def addTracksFromDirectory(directory, extensions=None):
    extensionList = extensions or ['.mp3','.wav','.ogg', '.flac']

    def readMetadata(files):
        albums = {}
        for f in files:
            try:
                md = MetaDataParser(f)
                if not albums.has_key(md.album()):
                    albums[md.album()] = []
                albums[md.album()].append(md)
            except mutagen.mp3.error:
                pass # borked file

        return albums

    tracks = []
    for root, dirs, files in os.walk(directory):
        for _f in files:
            if os.path.splitext(_f.lower())[1] in extensionList:
                tracks.append(os.path.join(root, _f))
        
    return addAlbums( readMetadata(tracks) )

class CoverModel(QtCore.QAbstractListModel):
    
    #Simple models can be created by subclassing this class and implementing
    #the minimum number of required functions. For example, we could implement a
    #simple read-only QStringList-based model that provides a list of strings to
    #a QListView widget. In such a case, we only need to implement the
    #rowCount() function to return the number of items in the list, and the
    #data() function to retrieve items from the list.  Since the model
    #represents a one-dimensional structure, the rowCount() function returns the
    #total number of items in the model. The columnCount() function is
    #implemented for interoperability with all kinds of views, but by default
    #informs views that the model contains only one column.

    def __init__(self, parent=None):
        super(CoverModel, self).__init__(parent)
        self.albums = []
        self.totalAlbums = totalAlbumCount()
        self.dataHasntBeenFetched = True

    def rowCount(self, parentidx):
        #print "rowcount: ", parentidx, ": ", len(self.albums)
        return len(self.albums)

    def data(self, idx, role):
        #print "data: ",idx, role
        #The general purpose roles are:
        #Qt::DisplayRole  0 The key data to be rendered (usually text).
        #Qt::DecorationRole 1 The data to be rendered as a decoration (usually an icon).
        #Qt::EditRole 2 The data in a form suitable for editing in an editor.
        #Qt::ToolTipRole  3 The data displayed in the item's tooltip.
        #Qt::StatusTipRole  4 The data displayed in the status bar.
        #Qt::WhatsThisRole  5 The data displayed for the item in "What's This?" mode.
        #Qt::SizeHintRole 13  The size hint for the item that will be supplied to views.

        #Roles describing appearance and meta data:
        #Qt::FontRole 6 The font used for items rendered with the default delegate.
        #Qt::TextAlignmentRole  7 The alignment of the text for items rendered with the default delegate.
        #Qt::BackgroundRole 8 The background brush used for items rendered with the default delegate.
        #Qt::BackgroundColorRole  8 This role is obsolete. Use BackgroundRole instead.
        #Qt::ForegroundRole 9 The foreground brush (text color, typically) used for items rendered with the default delegate.
        #Qt::TextColorRole  9 This role is obsolete. Use ForegroundRole instead.
        #Qt::CheckStateRole 10  This role is used to obtain the checked state of an item (see Qt::CheckState).

        #Accessibility roles:
        #Qt::AccessibleTextRole 11  The text to be used by accessibility extensions and plugins, such as screen readers.
        #Qt::AccessibleDescriptionRole  12  A description of the item for accessibility purposes.

        #User roles:
        #Qt::UserRole 32  The first role that can be used for application-specific purposes.

        if not idx.isValid():
            return QtCore.QVariant()

        #print "idxrow: ", idx.row()
        try:
            album = self.albums[idx.row()]
        except IndexError:
            print "inxesxxerror: ", idx.row()
            return QtCore.QVariant()

        if role in ( QtCore.Qt.DisplayRole, QtCore.Qt.ToolTipRole ):
            return QtCore.QVariant(album['title'])

        elif role == QtCore.Qt.DecorationRole:
            coverPix = QtGui.QPixmap()
            coverPix.loadFromData(album['cover'])
            return coverPix.scaled(200,200, QtCore.Qt.KeepAspectRatio, QtCore.Qt.SmoothTransformation)

        elif role == QtCore.Qt.SizeHintRole:
            return QtCore.QSize(200,200)
        elif role == QtCore.Qt.TextAlignmentRole:
            return QtCore.Qt.AlignCenter
        #elif role == QtCore.Qt.TextColorRole:
            #return QtGui.QColor(255,255,255,177)
        else:
            return QtCore.QVariant()
            
    def headerData(self, section, orientation, role):
        #print "headerData", section, orientation, role
        if role != QtCore.Qt.DisplayRole:
            return QtCore.QVariant()

    def canFetchMore(self, idx):
        return self.dataHasntBeenFetched or self.albums < self.totalAlbums

    def fetchMore(self, parentidx):
        #print "fetchmore: ", parentidx
        self.layoutAboutToBeChanged.emit()
        self.albums = list(itertools.chain.from_iterable([self.albums, loadAlbums(len(self.albums), 15)]))
        self.layoutChanged.emit()
        #print "albums;: ", self.albums
        self.dataHasntBeenFetched = False

    def getAlbum(self, idx):
        return self.albums[idx.row()]

class MediaObjectQueueModel(QtCore.QAbstractListModel):
    def __init__(self, mediaObject, parent=None):
        super(MediaObjectQueueModel, self).__init__(parent)
        self.mediaObject = mediaObject

    def rowCount(self, parentidx):
        #print "rowcount: ",parentidx, len(self.mediaObject.queue())
        return len(self.mediaObject.queue()) + 1

    def rowsChanged(self):
        self.layoutChanged.emit()

    def data(self, idx, role):
        #print "data: ",idx, role
        #The general purpose roles are:
        #Qt::DisplayRole  0 The key data to be rendered (usually text).
        #Qt::DecorationRole 1 The data to be rendered as a decoration (usually an icon).
        #Qt::EditRole 2 The data in a form suitable for editing in an editor.
        #Qt::ToolTipRole  3 The data displayed in the item's tooltip.
        #Qt::StatusTipRole  4 The data displayed in the status bar.
        #Qt::WhatsThisRole  5 The data displayed for the item in "What's This?" mode.
        #Qt::SizeHintRole 13  The size hint for the item that will be supplied to views.

        #Roles describing appearance and meta data:
        #Qt::FontRole 6 The font used for items rendered with the default delegate.
        #Qt::TextAlignmentRole  7 The alignment of the text for items rendered with the default delegate.
        #Qt::BackgroundRole 8 The background brush used for items rendered with the default delegate.
        #Qt::BackgroundColorRole  8 This role is obsolete. Use BackgroundRole instead.
        #Qt::ForegroundRole 9 The foreground brush (text color, typically) used for items rendered with the default delegate.
        #Qt::TextColorRole  9 This role is obsolete. Use ForegroundRole instead.
        #Qt::CheckStateRole 10  This role is used to obtain the checked state of an item (see Qt::CheckState).

        #Accessibility roles:
        #Qt::AccessibleTextRole 11  The text to be used by accessibility extensions and plugins, such as screen readers.
        #Qt::AccessibleDescriptionRole  12  A description of the item for accessibility purposes.

        #User roles:
        #Qt::UserRole 32  The first role that can be used for application-specific purposes.

        if not idx.isValid():
            return QtCore.QVariant()

        #print "idxrow: ", idx.row()
        if idx.row() == 0: #special case, return current playing item
            qrow = self.mediaObject.currentSource()
            #print "current: ", qrow
            currentPlaying = True
        else:
            currentPlaying = False
            try:
                qrow = self.mediaObject.queue()[idx.row()-1] # get enqueued tracks from player, and offset for current playing item
            except IndexError:
                print "inxesxxerror: ", idx.row()
                return QtCore.QVariant()

        try:
            md = MetaDataParser(qrow.fileName())
        except IOError:
            # no current (valid) source
            return QtCore.QVariant()

        if role in ( QtCore.Qt.DisplayRole, QtCore.Qt.ToolTipRole ):
            return QtCore.QVariant(md.title())

        elif currentPlaying == True and role == QtCore.Qt.ForegroundRole:
            return QtGui.QColor('lightGray')
        else:
            return QtCore.QVariant()
            
    def headerData(self, section, orientation, role):
        #print "headerData", section, orientation, role
        if role != QtCore.Qt.DisplayRole:
            return QtCore.QVariant()

class MainWindow(QtGui.QMainWindow):
    def __init__(self):
        super(QtGui.QMainWindow, self).__init__()

        self.audioOutput = Phonon.AudioOutput(Phonon.MusicCategory, self)
        self.mediaObject = Phonon.MediaObject(self)
        self.metaInformationResolver = Phonon.MediaObject(self)

        self.mediaObject.setTickInterval(1000)

        self.mediaObject.tick.connect(self.tick)
        self.mediaObject.stateChanged.connect(self.stateChanged)
        self.metaInformationResolver.stateChanged.connect(self.metaStateChanged)
        self.mediaObject.currentSourceChanged.connect(self.sourceChanged)
        self.mediaObject.aboutToFinish.connect(self.aboutToFinish)

        Phonon.createPath(self.mediaObject, self.audioOutput)

        self.setupActions()
        self.setupMenus()
        self.setupUi()

        self.sources = []

    def sizeHint(self):
        return QtCore.QSize(1000, 1300)

    def addFiles(self, files):
        index = len(self.sources)

        for string in files:
            self.sources.append(Phonon.MediaSource(string))

        if self.sources:
            self.metaInformationResolver.setCurrentSource(self.sources[index])

    def about(self):
        QtGui.QMessageBox.information(self, "About Fingerspill",
                "The Music Player example shows how to use Phonon - the "
                "multimedia framework that comes with Qt - to create a "
                "simple music player.")

    def stateChanged(self, newState, oldState):
        if newState == Phonon.ErrorState and self.mediaObject.currentSource().fileName() != '':
            if self.mediaObject.errorType() == Phonon.FatalError:
                QtGui.QMessageBox.warning(self, "Fatal Error",
                        self.mediaObject.errorString())
            else:
                QtGui.QMessageBox.warning(self, "Error",
                        self.mediaObject.errorString())

        elif newState == Phonon.PlayingState:
            self.playAction.setEnabled(False)
            self.pauseAction.setEnabled(True)
            self.stopAction.setEnabled(True)
            self.nextAction.setEnabled(True)

        elif newState == Phonon.StoppedState:
            self.stopAction.setEnabled(False)
            self.playAction.setEnabled(True)
            self.pauseAction.setEnabled(False)
            self.nextAction.setEnabled(False)

        elif newState == Phonon.PausedState:
            self.pauseAction.setEnabled(False)
            self.stopAction.setEnabled(True)
            self.playAction.setEnabled(True)
            self.nextAction.setEnabled(True)

    def tick(self, time):
        displayTime = QtCore.QTime(0, (time / 60000) % 60, (time / 1000) % 60)

    def tableClicked(self, row, column):
        wasPlaying = (self.mediaObject.state() == Phonon.PlayingState)

        self.mediaObject.stop()
        self.mediaObject.clearQueue()

        self.mediaObject.setCurrentSource(self.sources[row])

        if wasPlaying:
            self.mediaObject.play()
        else:
            self.mediaObject.stop()

    def albumActivated(self, idx):
        wasPlaying = (self.mediaObject.state() == Phonon.PlayingState)

        self.mediaObject.stop()
        self.mediaObject.clear()
        
        tracks = sorted(self.albumStore.getAlbum(idx)['tracks'], key=operator.itemgetter('trackno'))
        print "tracks: ", tracks
        self.mediaObject.enqueue( list( [ Phonon.MediaSource(t['path']) for t in tracks ] ) )
        self.mediaObjectQueue.rowsChanged()

        if wasPlaying:
            self.mediaObject.play()
        else:
            self.mediaObject.stop()

    def sourceChanged(self, source):
        try:
            md = MetaDataParser(self.mediaObject.currentSource().fileName())
            self.nowPlayingLabel.setText('%s - %s' % (md.artist(), md.title()))
        except IOError:
            self.nowPlayingLabel.setText('')
        if len(self.mediaObject.queue()) == 0: # no next item queued
            self.nextAction.setEnabled(False)

    def skipForward(self):
        wasPlaying =  self.mediaObject.state() == Phonon.PlayingState # if it is currently playing, remember that
        q = self.mediaObject.queue()
        self.mediaObject.setCurrentSource(q[0])
        self.mediaObject.setQueue(q[1:])
        if wasPlaying:
           self.mediaObject.play() 

    def metaStateChanged(self, newState, oldState):
        if newState == Phonon.ErrorState:
            QtGui.QMessageBox.warning(self, "Error opening files",
                    self.metaInformationResolver.errorString())

            while self.sources and self.sources.pop() != self.metaInformationResolver.currentSource():
                pass

            return

        if newState != Phonon.StoppedState and newState != Phonon.PausedState:
            return

        if self.metaInformationResolver.currentSource().type() == Phonon.MediaSource.Invalid:
            return

        metaData = MetaDataParser(self.metaInformationResolver.currentSource().fileName())

        title = metaData.title()

        titleItem = QtGui.QTableWidgetItem(title)
        titleItem.setFlags(titleItem.flags() ^ QtCore.Qt.ItemIsEditable)

        artist = metaData.artist()
        artistItem = QtGui.QTableWidgetItem(artist)
        artistItem.setFlags(artistItem.flags() ^ QtCore.Qt.ItemIsEditable)

        album = metaData.album()
        albumItem = QtGui.QTableWidgetItem(album)
        albumItem.setFlags(albumItem.flags() ^ QtCore.Qt.ItemIsEditable)

        cover = metaData.cover()
        if cover is not None: 
            coverItem = QtGui.QTableWidgetItem()
            coverItem.setIcon(coverIcon)
            coverItem.setFlags(coverItem.flags() ^ QtCore.Qt.ItemIsEditable)

        currentRow = self.musicTable.rowCount()
        self.musicTable.insertRow(currentRow)
        self.musicTable.setItem(currentRow, 0, titleItem)
        self.musicTable.setItem(currentRow, 1, artistItem)
        self.musicTable.setItem(currentRow, 2, albumItem)
        if cover is not None:
            self.musicTable.setItem(currentRow, 3, coverItem)

        if not self.musicTable.selectedItems():
            #self.musicTable.selectRow(0)
            self.mediaObject.setCurrentSource(self.metaInformationResolver.currentSource())

        index = self.sources.index(self.metaInformationResolver.currentSource()) + 1

        if len(self.sources) > index:
            self.metaInformationResolver.setCurrentSource(self.sources[index])
        else:
            self.musicTable.resizeColumnsToContents()
            if self.musicTable.columnWidth(0) > 300:
                self.musicTable.setColumnWidth(0, 300)

    def aboutToFinish(self):
        index = self.sources.index(self.mediaObject.currentSource()) + 1
        if len(self.sources) > index:
            self.mediaObject.enqueue(self.sources[index])

    def setupActions(self):
        self.playAction = QtGui.QAction(
                self.style().standardIcon(QtGui.QStyle.SP_MediaPlay), "Play",
                self, shortcut="Ctrl+P", enabled=False,
                triggered=self.mediaObject.play)

        self.pauseAction = QtGui.QAction(
                self.style().standardIcon(QtGui.QStyle.SP_MediaPause),
                "Pause", self, shortcut="Ctrl+A", enabled=False,
                triggered=self.mediaObject.pause)

        self.stopAction = QtGui.QAction(
                self.style().standardIcon(QtGui.QStyle.SP_MediaStop), "Stop",
                self, shortcut="Ctrl+S", enabled=False,
                triggered=self.mediaObject.stop)

        self.nextAction = QtGui.QAction(
                self.style().standardIcon(QtGui.QStyle.SP_MediaSkipForward),
                "Next", self, shortcut="Ctrl+N", enabled=False,
                triggered=self.skipForward)

        self.previousAction = QtGui.QAction(
                self.style().standardIcon(QtGui.QStyle.SP_MediaSkipBackward),
                "Previous", self, shortcut="Ctrl+R")

        self.exitAction = QtGui.QAction("E&xit", self, shortcut="Ctrl+X",
                triggered=self.close)

        self.aboutAction = QtGui.QAction("A&bout", self, shortcut="Ctrl+B",
                triggered=self.about)

        self.aboutQtAction = QtGui.QAction("About &Qt", self,
                shortcut="Ctrl+Q", triggered=QtGui.qApp.aboutQt)

    def setupMenus(self):
        fileMenu = self.menuBar().addMenu("&File")
        fileMenu.addSeparator()
        fileMenu.addAction(self.exitAction)

        aboutMenu = self.menuBar().addMenu("&Help")
        aboutMenu.addAction(self.aboutAction)
        aboutMenu.addAction(self.aboutQtAction)

    def setupUi(self):
        bar = QtGui.QToolBar()

        bar.addAction(self.playAction)
        bar.addAction(self.pauseAction)
        bar.addAction(self.stopAction)
        bar.addAction(self.nextAction)

        self.seekSlider = Phonon.SeekSlider(self)
        self.seekSlider.setMediaObject(self.mediaObject)

        self.volumeSlider = Phonon.VolumeSlider(self)
        self.volumeSlider.setAudioOutput(self.audioOutput)
        self.volumeSlider.setSizePolicy(QtGui.QSizePolicy.Maximum,
                QtGui.QSizePolicy.Maximum)

        self.nowPlayingLabel = QtGui.QLabel()

        volumeLabel = QtGui.QLabel()
        volumeLabel.setPixmap(QtGui.QPixmap('images/volume.png'))

        palette = QtGui.QPalette()
        palette.setBrush(QtGui.QPalette.Light, QtCore.Qt.darkGray)

        self.albumStore = CoverModel()

        self.musicTable = QtGui.QListView(self)
        self.musicTable.setViewMode(QtGui.QListView.IconMode)
        self.musicTable.setModel(self.albumStore)
        #self.musicTable.cellPressed.connect(self.tableClicked)
        self.musicTable.activated.connect(self.albumActivated)
        self.musicTable.clicked.connect(self.albumActivated)
        self.musicTable.setMinimumSize(800,600)
        self.musicTable.setSizePolicy(QtGui.QSizePolicy.MinimumExpanding,
                QtGui.QSizePolicy.MinimumExpanding)

        self.mediaObjectQueue = MediaObjectQueueModel(self.mediaObject)
        self.playlist = QtGui.QListView(self)
        self.playlist.setModel(self.mediaObjectQueue)
        self.playlist.sizeHint = QtCore.QSize(200,600)
        self.musicTable.setSizePolicy(QtGui.QSizePolicy.MinimumExpanding,
                QtGui.QSizePolicy.Expanding)


        musicLayout = QtGui.QHBoxLayout()
        musicLayout.addWidget(self.musicTable)
        musicLayout.addStretch()
        musicLayout.addWidget(self.playlist)

        seekerLayout = QtGui.QHBoxLayout()
        seekerLayout.addWidget(self.seekSlider)

        playbackLayout = QtGui.QHBoxLayout()
        playbackLayout.addWidget(bar)
        playbackLayout.addStretch()
        playbackLayout.addWidget(self.nowPlayingLabel)
        playbackLayout.addStretch()
        playbackLayout.addWidget(volumeLabel)
        playbackLayout.addWidget(self.volumeSlider)

        mainLayout = QtGui.QVBoxLayout()
        mainLayout.addLayout(musicLayout)
        mainLayout.addLayout(seekerLayout)
        mainLayout.addLayout(playbackLayout)

        widget = QtGui.QWidget()
        widget.setLayout(mainLayout)

        self.setCentralWidget(widget)
        self.setWindowTitle("Fingerspill Music Player")


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    app.setApplicationName("Music Player")
    app.setQuitOnLastWindowClosed(True)

    window = MainWindow()
    window.show()

    addTracksFromDirectory(os.path.join(os.path.dirname(__file__), 'library'))
    sys.exit(app.exec_())
